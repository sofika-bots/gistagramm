function process_gist() {
    var gists = document.getElementsByClassName("gistagramm");
    for (var i = 0; i < gists.length; i++) {
        var cols = gists[i].getElementsByClassName("coloumn");
        var sum = 0;
        for (var j = 0; j < cols.length; j++) {
            sum += parseInt(cols[j].dataset.val);
        }
        for (var j = 0; j < cols.length; j++) {
            cols[j].style.height = parseInt(cols[j].dataset.val) / sum * 90 + "%";
            cols[j].style.marginTop = parseInt(cols[j].dataset.val) / sum * 90 + "%"; 
            cols[j].innerHTML = "<p style='margin-top: -20px'>" + cols[j].dataset.val + "</p>";
        }
    }
}

window.onload = function() {
    process_gist();
}